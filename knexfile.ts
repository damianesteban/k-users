module.exports = {
  development: {
    client: 'mysql',
    connection: {
      charset: 'utf8',
      timezone: 'UTC',
      host: 'database',
      port: 3306,
      database: 'testeri',
      user: 'root',
      password: 'prisma',
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      directory: 'src/database/migrations',
      tableName: 'migrations',
    },
    seeds: {
      directory: 'src/database/seeds',
    },
  },
};
