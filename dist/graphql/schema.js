"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_1 = require("apollo-server");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const signale = require("signale");
const index_1 = require("../services/index");
const createToken = (id, email, expiresIn) => __awaiter(this, void 0, void 0, function* () {
    return yield jwt.sign({ id, email }, 'secret', { expiresIn });
});
const validatePassword = (originalPassword, password) => __awaiter(this, void 0, void 0, function* () {
    return yield bcrypt.compare(originalPassword, password);
});
const schema = apollo_server_1.gql `
  type Query {
    me: User
    user(id: ID!): User
    project(id: ID!): Project
    projectByTitle(title: String!): Project
    projects: [Project!]
    users: [User!]
  }

  type User {
    id: ID!
    email: String!
    password: String!
    projects: [Project]
  }

  type Project {
    id: ID!
    user: User!
    imageUrlString: String
    title: String!
  }

  type Mutation {
    createProject(title: String!, imageUrlString: String!, userId: Int!): Project!
    signUp(email: String!, password: String!): Token!
    signIn(email: String!, password: String!): Token!
  }

  type Token {
    token: String!
  }
`;
exports.schema = schema;
const resolvers = {
    Query: {
        me(root, args, context, info) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const me = yield index_1.User.fetchMeByEmail();
                    return me;
                }
                catch (error) {
                    throw error;
                }
            });
        },
        user(root, args, context, info) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const user = index_1.User.fetchById(args, context);
                    return user;
                }
                catch (error) {
                    signale.fatal('individual comic query error', error);
                    throw error;
                }
            });
        },
        project(root, args, context, info) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const project = index_1.Project.fetchById(args, context);
                    return project;
                }
                catch (error) {
                    signale.fatal('individual comic query error', error);
                    throw error;
                }
            });
        },
        projects(root, args, context, info) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const projects = yield index_1.Project.loadAll();
                    return projects;
                }
                catch (error) {
                    throw error;
                }
            });
        },
        projectByTitle(root, args, context, info) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const project = yield index_1.Project.fetchByTitle(args, context);
                    return project;
                }
                catch (error) {
                    throw error;
                }
            });
        },
        users(root, args, context, info) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const users = yield index_1.User.loadAll();
                    return users;
                }
                catch (error) {
                    throw error;
                }
            });
        },
    },
    Mutation: {
        signUp(root, { email, password }, context, info) {
            return __awaiter(this, void 0, void 0, function* () {
                const user = yield index_1.User.create({ email, password }, context);
                const token = yield createToken(user.id, user.email, '1000m');
                return { token };
            });
        },
        signIn(root, { email, password }, context, info) {
            return __awaiter(this, void 0, void 0, function* () {
                const user = yield index_1.User.fetchByEmail({ email }, context);
                if (!user) {
                    throw Error('No user found with this login credentials.');
                }
                const valid = yield validatePassword(user.password, password);
                if (!valid)
                    throw Error('password invalid');
                const token = yield createToken(user.id, user.email, '1000m');
                return { token };
            });
        },
        createProject(root, args, context, info) {
            return __awaiter(this, void 0, void 0, function* () {
                const project = yield index_1.Project.create(args, context);
                return project;
            });
        },
    },
};
exports.resolvers = resolvers;
//# sourceMappingURL=schema.js.map