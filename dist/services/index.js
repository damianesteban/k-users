"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("../db");
class UserQuery {
    static create({ email, password }) {
        return __awaiter(this, void 0, void 0, function* () {
            return db_1.knex
                .insert({ email, password })
                .into('users');
        });
    }
    static fetchById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return db_1.knex
                .first()
                .table('users')
                .where('id', id);
        });
    }
    static fetchByIds(ids) {
        return __awaiter(this, void 0, void 0, function* () {
            return db_1.knex
                .select()
                .table('users')
                .whereIn('id', ids);
        });
    }
    static fetchAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return db_1.knex
                .select()
                .table('users');
        });
    }
    static fetchByEmail(email) {
        return __awaiter(this, void 0, void 0, function* () {
            return db_1.knex
                .first()
                .table('users')
                .where('email', email);
        });
    }
}
exports.UserQuery = UserQuery;
class ProjectQuery {
    static create({ userId, title, imageUrlString }) {
        return __awaiter(this, void 0, void 0, function* () {
            return db_1.knex
                .insert({ userId, title, imageUrlString })
                .into('projects');
        });
    }
    static fetchById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return db_1.knex
                .first()
                .table('projects')
                .where('id', id);
        });
    }
    static fetchByIds(ids) {
        return __awaiter(this, void 0, void 0, function* () {
            return db_1.knex
                .select()
                .table('projects')
                .whereIn('id', ids);
        });
    }
    static fetchAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return db_1.knex
                .select()
                .table('projects');
        });
    }
    static fetchByTitle(title) {
        return __awaiter(this, void 0, void 0, function* () {
            return db_1.knex
                .first()
                .table('projects')
                .where('title', title);
        });
    }
}
exports.ProjectQuery = ProjectQuery;
class User {
    constructor(data) {
        this.id = data.id;
        this.email = data.email;
        this.password = data.lastName;
    }
    static create(args, ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield UserQuery.create({ email: args.email, password: args.password });
            if (!data)
                return null;
            return data;
        });
    }
    static load(args, ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield UserQuery.fetchById(args.id);
            if (!data)
                return null;
            return Promise.resolve(new User(data));
        });
    }
    static fetchByEmail(args, ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield UserQuery.fetchByEmail(args.email);
            if (!data)
                return null;
            return Promise.resolve(new User(data));
        });
    }
    static fetchMeByEmail(email = 'me@balls.com') {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield UserQuery.fetchByEmail(email);
            if (!data)
                return null;
            return Promise.resolve(new User(data));
        });
    }
    static fetchById(args, context) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield UserQuery.fetchById(args.id);
            if (!data)
                return null;
            return Promise.resolve(new User(data));
        });
    }
    static fetchByIds(args, ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield UserQuery.fetchByIds(args.ids);
            if (!data)
                return null;
            const userData = data.map(row => new User(row));
            return Promise.resolve(userData);
        });
    }
    static loadAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield UserQuery.fetchAll();
            const userData = data.map(row => new User(row));
            return Promise.resolve(userData);
        });
    }
}
exports.User = User;
class Project {
    constructor(data) {
        this.id = data.id;
        this.userId = data.userId;
        this.title = data.title;
        this.imageUrlString = data.imageUrlString;
    }
    static create(args, ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield ProjectQuery.create({ title: args.email, userId: args.userId, imageUrlString: args.imageUrlString });
            if (!data)
                return null;
            return data;
        });
    }
    static load(args, ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield ProjectQuery.fetchById(args.id);
            if (!data)
                return null;
            return Promise.resolve(new Project(data));
        });
    }
    static fetchByTitle(args, ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield ProjectQuery.fetchByTitle(args.title);
            if (!data)
                return null;
            return Promise.resolve(new Project(data));
        });
    }
    static fetchById(args, context) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield ProjectQuery.fetchById(args.id);
            if (!data)
                return null;
            return Promise.resolve(new Project(data));
        });
    }
    static fetchByIds(args, ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield ProjectQuery.fetchByIds(args.ids);
            if (!data)
                return null;
            const projectData = data.map(row => new Project(row));
            return Promise.resolve(projectData);
        });
    }
    static loadAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield ProjectQuery.fetchAll();
            const projectData = data.map(row => new Project(row));
            return Promise.resolve(projectData);
        });
    }
}
exports.Project = Project;
//# sourceMappingURL=index.js.map