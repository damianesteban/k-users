"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_1 = require("apollo-server");
const schema_1 = require("./graphql/schema");
const signale = require("signale");
function startServer() {
    return __awaiter(this, void 0, void 0, function* () {
        const server = new apollo_server_1.ApolloServer({
            typeDefs: schema_1.schema,
            resolvers: schema_1.resolvers,
            engine: {
                apiKey: 'service:knex-test:c1Wm2W1812Ixo83F5aDr-A',
            },
        });
        server.listen(4000, (info) => __awaiter(this, void 0, void 0, function* () {
            try {
                console.log('NEW BUILD!!!!!!!!!!!!!!!!!!!!!!!');
            }
            catch (error) {
                signale.error(`🔥  MySQL error: ${error.message}`);
                throw error;
            }
            signale.info(`🚀 Server ready at ${info}`);
        }));
    });
}
startServer().catch(error => signale.error(error));
//# sourceMappingURL=index.js.map