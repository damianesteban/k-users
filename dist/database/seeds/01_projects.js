"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_1 = require("../data");
exports.seed = function (knex) {
    return knex('projects').del()
        .then(() => {
        knex('projects').insert(data_1.data.projects);
    });
};
//# sourceMappingURL=01_projects.js.map