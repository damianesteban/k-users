"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_1 = require("../data");
const signale = require("signale");
exports.seed = function (knex) {
    return knex('users').del()
        .then(() => {
        knex('users').insert(data_1.data.users);
        signale.debug('users seed ran!');
    });
};
//# sourceMappingURL=01_users.js.map