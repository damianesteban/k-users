"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const faker = require("faker");
exports.data = {
    users: [
        {
            email: faker.internet.email(),
            password: faker.internet.password(),
        },
        {
            email: faker.internet.email(),
            password: faker.internet.password(),
        },
        {
            email: faker.internet.email(),
            password: faker.internet.password(),
        },
        {
            email: faker.internet.email(),
            password: faker.internet.password(),
        },
        {
            email: faker.internet.email(),
            password: faker.internet.password(),
        },
    ],
    projects: [
        {
            userId: 1,
            title: faker.hacker.phrase(),
            imageUrlString: 'https://source.unsplash.com/210x195',
        },
        {
            userId: 1,
            title: faker.hacker.phrase(),
            imageUrlString: 'https://source.unsplash.com/195x200',
        },
        {
            userId: 2,
            title: faker.hacker.phrase(),
            imageUrlString: 'https://source.unsplash.com/205x200',
        },
        {
            userId: 3,
            title: faker.hacker.phrase(),
            imageUrlString: 'https://source.unsplash.com/200x205',
        },
    ],
};
//# sourceMappingURL=data.js.map