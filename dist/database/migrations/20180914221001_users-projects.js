"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.up = function (knex) {
    return knex.schema.createTable('users', (usersTable) => {
        usersTable.increments();
        usersTable.string('email', 250).notNullable().unique();
        usersTable.string('password', 128).notNullable();
        usersTable.timestamps(true, true);
    })
        .createTable('projects', (projectsTable) => {
        projectsTable.increments();
        projectsTable.integer('userId').unsigned().notNullable().references('id').inTable('users').onDelete('CASCADE');
        projectsTable.string('title').notNullable();
        projectsTable.string('imageUrlString', 250).nullable();
        projectsTable.timestamps(true, true);
    });
};
exports.down = function (knex) {
    return Promise.all([knex.schema.dropTable('users'), knex.schema.dropTable('projects')]);
};
//# sourceMappingURL=20180914221001_users-projects.js.map