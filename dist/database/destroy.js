"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Knex = require("knex");
const signale = require("signale");
const dbConfig = {
    client: 'mysql',
    connection: {
        charset: 'utf8',
        timezone: 'UTC',
        host: 'localhost',
        port: 3306,
        database: 'testeri',
        user: 'root',
        password: 'prisma',
    },
};
exports.knex = Knex(dbConfig);
const destroy = (knex) => Promise.all([knex.schema.dropTable('users'), knex.schema.dropTable('projects')]);
destroy(exports.knex)
    .then(() => signale.debug('Tables destroyed'));
//# sourceMappingURL=destroy.js.map