"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Knex = require("knex");
const data_1 = require("./database/data");
const signale = require("signale");
const dbConfig = {
    client: 'mysql',
    connection: {
        charset: 'utf8',
        timezone: 'UTC',
        host: 'localhost',
        port: 3306,
        database: 'testero',
        user: 'root',
        password: 'prisma',
    },
};
exports.knex = Knex(dbConfig);
exports.seed = () => {
    signale.debug('RUNNING SEED!!!!!');
    const userSeed = () => exports.knex('users').insert(data_1.data.users);
    const projectSeed = () => exports.knex('projects').insert(data_1.data.projects);
    return Promise.all([userSeed(), projectSeed()])
        .then(() => {
        console.log('Seed Complete');
    })
        .catch(err => console.log('Seed failed', err));
};
exports.userQuery = () => exports.knex.select().from('users');
exports.projectsQuery = () => exports.knex.select().from('projects');
//# sourceMappingURL=db.js.map