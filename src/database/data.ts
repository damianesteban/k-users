import * as faker from 'faker';

export const data = {
  users: [
    {
      id: 1,
      email: faker.internet.email(),
      password: faker.internet.password(),
    },
    {
      id: 2,
      email: faker.internet.email(),
      password: faker.internet.password(),
    },
    {
      id: 3,
      email: faker.internet.email(),
      password: faker.internet.password(),
    },
    {
      id: 4,
      email: faker.internet.email(),
      password: faker.internet.password(),
    },
    {
      id: 5,
      email: faker.internet.email(),
      password: faker.internet.password(),
    },
  ],
  projects: [
    {
      userId: 1,
      title: faker.hacker.phrase(),
      imageUrlString: 'https://source.unsplash.com/210x195',
    },
    {
      userId: 1,
      title: faker.hacker.phrase(),
      imageUrlString: 'https://source.unsplash.com/195x200',
    },
    {
      userId: 2,
      title: faker.hacker.phrase(),
      imageUrlString: 'https://source.unsplash.com/205x200',
    },
    {
      userId: 3,
      title: faker.hacker.phrase(),
      imageUrlString: 'https://source.unsplash.com/200x205',
    },
  ],
};
