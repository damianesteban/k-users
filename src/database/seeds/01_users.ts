import * as Knex from 'knex';
import { data } from '../data';
import * as signale from 'signale';
import * as Bluebird from 'bluebird';

exports.seed = async function (knex: Knex): Promise<void> {


  await knex('projects').del().then(() => console.log('del'));
  await knex('users').del().then(() => console.log('del'))
  await knex('users').insert(data.users).then(() => console.log('del'));
  await knex('projects').insert(data.projects).then(() => console.log('del'));
  
//  return Bluebird.all([seedUsers(), seedProjects()]);
};
