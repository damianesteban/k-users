import * as Knex from 'knex';
import * as signale from 'signale';

const dbConfig = {
  client: 'mysql',
  connection: {
    charset: 'utf8',
    timezone: 'UTC',
    host: 'localhost',
    port: 3306,
    database: 'testeri',
    user: 'root',
    password: 'prisma',
  },
};

export const knex = Knex(dbConfig);

const destroy = (knex: Knex) =>
  Promise.all([knex.schema.dropTable('users'), knex.schema.dropTable('projects')]);

destroy(knex)
  .then(() => signale.debug('Tables destroyed'))