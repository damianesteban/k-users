
import * as Knex from 'knex';

exports.up = function (knex: Knex) {
  return knex.schema.createTable('users', (usersTable) => {
    usersTable.increments();

    usersTable.string('email', 250).notNullable().unique();
    usersTable.string('password', 128).notNullable();

    usersTable.timestamps(true, true);
  })
  .createTable('projects', (projectsTable) => {

    // Primary Key
    projectsTable.increments();
    // tslint:disable-next-line:max-line-length
    projectsTable.integer('userId').unsigned().notNullable().references('id').inTable('users').onDelete('CASCADE');
    projectsTable.string('title').notNullable();
    projectsTable.string('imageUrlString', 250).nullable();

    projectsTable.timestamps(true, true);

  });
};

exports.down = function (knex: Knex) {
  return Promise.all([knex.schema.dropTable('users'), knex.schema.dropTable('projects')]);
};
