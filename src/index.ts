import {  ApolloServer } from 'apollo-server';
import { userQuery, projectsQuery } from './db';
import { schema, resolvers } from './graphql/schema';
import * as signale from 'signale';

// const PORT = process.env.PORT || 3000;

async function startServer() {

  const server: ApolloServer = new ApolloServer({
    typeDefs: schema, 
    // tslint:disable-next-line:object-shorthand-properties-first
    resolvers,
    engine: {
      apiKey: 'service:knex-test:c1Wm2W1812Ixo83F5aDr-A',
    },
  });

  server.listen(4000, async (info) => {
    try {
      console.log('NEW BUILD!!!!!!!!!!!!!!!!!!!!!!!');
    } catch (error) {
      signale.error(`🔥  MySQL error: ${error.message}`);
      throw error;
    }
    signale.info(`🚀 Server ready at ${info}`);
  });
}

startServer().catch(error => signale.error(error));
