import { knex } from '../db';
import * as Dataloader from 'dataloader';

export class UserQuery {

  static async create({ email, password }) {
    return knex
      .insert({ email, password })
      .into('users');
  }

  static async fetchById(id: number) {
    return knex
      .first()
      .table('users')
      .where('id', id);
  }

  static async fetchByIds(ids: number[]) {
    return knex
    .select()
    .table('users')
    .whereIn('id', ids);
  }

  static async fetchAll() {
    return knex
    .select()
    .table('users');
  }

  static async fetchByEmail(email: string) {
    return knex
      .first()
      .table('users')
      .where('email', email);
  }
}

export class ProjectQuery {

  static async create({ userId, title, imageUrlString }) {
    return knex
      .insert({ userId, title, imageUrlString })
      .into('projects');
  }

  static async fetchById(id: number) {
    return knex
      .first()
      .table('projects')
      .where('id', id);
  }

  static async fetchByIds(ids: number[]) {
    return knex
    .select()
    .table('projects')
    .whereIn('id', ids);
  }

  static async fetchAll() {
    return knex
    .select()
    .table('projects');
  }

  static async fetchByTitle(title: string) {
    return knex
      .first()
      .table('projects')
      .where('title', title);
  }
}

export class User {
  id: number;
  email: string;
  password: string;
​
  constructor(data) {
    this.id = data.id;
    this.email = data.email;
    this.password = data.lastName;
  }

  static async create(args: any, ctx: any): Promise<User> {
    const data = await UserQuery.create({ email: args.email, password: args.password });
    if (!data) return null;
​    return data;
  }

  static async load(args: any, ctx: any): Promise<User> {
    const data = await UserQuery.fetchById(args.id);
    if (!data) return null;
​
    return Promise.resolve(new User(data));
  }

  static async fetchByEmail(args: any, ctx: any): Promise<User> {
    const data = await UserQuery.fetchByEmail(args.email);
    if (!data) return null;

    return Promise.resolve(new User(data));
  }

  static async fetchMeByEmail(email: string = 'me@balls.com'): Promise<User> {
    const data = await UserQuery.fetchByEmail(email);
    if (!data) return null;

    return Promise.resolve(new User(data));
  }

  static async fetchById(args: any, context: any) {
    const data = await UserQuery.fetchById(args.id);
    if (!data) return null;

    return Promise.resolve(new User(data));
  }

  static async fetchByIds(args: any, ctx: any): Promise<User[]> {
    const data = await UserQuery.fetchByIds(args.ids);
    if (!data) return null;

    const userData = data.map(row => new User(row));
    return Promise.resolve(userData);
  }

  static async loadAll() {
    const data = await UserQuery.fetchAll();
    const userData = data.map(row => new User(row));
    return Promise.resolve(userData);
  }
}

export class Project {
  id: number;
  userId: number;
  title: string;
  imageUrlString: string;
​
  constructor(data) {
    this.id = data.id;
    this.userId = data.userId;
    this.title = data.title;
    this.imageUrlString = data.imageUrlString;
  }

  static async create(args: any, ctx: any): Promise<Project> {
    // tslint:disable-next-line:max-line-length
    const data = await ProjectQuery.create({ title: args.email, userId: args.userId, imageUrlString: args.imageUrlString });
    if (!data) return null;
​    return data;
  }

  static async load(args: any, ctx: any): Promise<Project> {
    const data = await ProjectQuery.fetchById(args.id);
    if (!data) return null;
​
    return Promise.resolve(new Project(data));
  }

  static async fetchByTitle(args: any, ctx: any): Promise<Project> {
    const data = await ProjectQuery.fetchByTitle(args.title);
    if (!data) return null;

    return Promise.resolve(new Project(data));
  }

  static async fetchById(args: any, context: any) {
    const data = await ProjectQuery.fetchById(args.id);
    if (!data) return null;

    return Promise.resolve(new Project(data));
  }

  static async fetchByIds(args: any, ctx: any): Promise<Project[]> {
    const data = await ProjectQuery.fetchByIds(args.ids);
    if (!data) return null;

    const projectData = data.map(row => new Project(row));
    return Promise.resolve(projectData);
  }

  static async loadAll() {
    const data = await ProjectQuery.fetchAll();
    const projectData = data.map(row => new Project(row));
    return Promise.resolve(projectData);
  }
}
