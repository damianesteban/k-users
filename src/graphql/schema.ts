import { gql } from 'apollo-server';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import * as signale from 'signale';
import { Project, User } from '../services/index';

const createToken = async (id: number, email: string, expiresIn: string) => {
  return await jwt.sign({ id, email }, 'secret', { expiresIn });
};

const validatePassword = async (originalPassword: string, password: string) => {
  return await bcrypt.compare(originalPassword, password);
};

const schema = gql`
  type Query {
    me: User
    user(id: ID!): User
    project(id: ID!): Project
    projectByTitle(title: String!): Project
    projects: [Project!]
    users: [User!]
  }

  type User {
    id: ID!
    email: String!
    password: String!
    projects: [Project]
  }

  type Project {
    id: ID!
    user: User!
    imageUrlString: String
    title: String!
  }

  type Mutation {
    createProject(title: String!, imageUrlString: String!, userId: Int!): Project!
    signUp(email: String!, password: String!): Token!
    signIn(email: String!, password: String!): Token!
  }

  type Token {
    token: String!
  }
`;

// A map of functions which return data for the schema.
const resolvers = {
  Query: {
    async me(root: any, args: any, context: any, info: any) {
      try {
        const me = await User.fetchMeByEmail();
        return me;
      } catch (error) {
        throw error;
      }
    },
    async user(root: any, args: any, context: any, info: any) {
      try {
        const user = User.fetchById(args, context);
        return user;
      } catch (error) {
        signale.fatal('individual comic query error', error);
        throw error;
      }
    },
    async project(root: any, args: any, context: any, info: any) {
      try {
        const project = Project.fetchById(args, context);
        return project;
      } catch (error) {
        signale.fatal('individual comic query error', error);
        throw error;
      }
    },
    async projects(root: any, args: any, context: any, info: any) {
      try {
        const projects = await Project.loadAll();
        return projects;
      } catch (error) {
        throw error;
      }
    },
    async projectByTitle(root: any, args: any, context: any, info: any) {
      try {
        const project = await Project.fetchByTitle(args, context);
        return project;
      } catch (error) {
        throw error;
      }
    },
    async users(root: any, args: any, context: any, info: any) {
      try {
        const users = await User.loadAll();
        return users;
      } catch (error) {
        throw error;
      }
    },
  },
  Mutation: {
    async signUp(
      root: any,
      { email, password },
      context: any,
      info: any,
    ) {
      const user = await User.create({ email, password }, context);
      const token = await createToken(user.id, user.email, '1000m');
      return { token };
    },
    async signIn(
      root: any,
      { email, password },
      context: any,
      info: any,
    ) {
      const user = await User.fetchByEmail({ email }, context);

      if (!user) {
        throw Error(
          'No user found with this login credentials.',
        );
      }

      const valid = await validatePassword(user.password, password);
      if (!valid) throw Error('password invalid');

      const token = await createToken(user.id, user.email, '1000m');
      return { token };
    },
    async createProject(
      root: any,
      args: any,
      context: any,
      info: any,
    ) {
      const project = await Project.create(args, context);
      return project;
    },
  },
};

export { schema, resolvers };
