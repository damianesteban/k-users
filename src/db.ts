import * as Knex from 'knex';
import { data } from './database/data';
import * as signale from 'signale';

const dbConfig = {
  client: 'mysql',
  connection: {
    charset: 'utf8',
    timezone: 'UTC',
    host: 'database',
    port: 3306,
    database: 'testeri',
    user: 'root',
    password: 'prisma',
  },
};

export const knex = Knex(dbConfig);

export const seed = () => {
    // Deletes ALL existing entries
  signale.debug('RUNNING SEED!!!!!');

  // const deleteUsers = () =>
  //   knex('users').where('id', '!=', '0').destroy()

  // const deleteProjects = () =>
  // knex('projects').where('id', '!=', '0').delete();
  const userSeed = () =>
    knex('users').insert(data.users);

  const projectSeed = () =>
      knex('projects').insert(data.projects);

  return Promise.all([userSeed(), projectSeed()])
    .then(() => {
      console.log('Seed Complete');
    })
    .catch(err => console.log('Seed failed', err));
};

export const userQuery = () =>
  knex.select().from('users');

export const projectsQuery = () =>
  knex.select().from('projects');
